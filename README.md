# Event Driven Ansible demo

## Overwiew

This is a simple demonstration with [Event Driven Ansible](https://github.com/ansible/event-driven-ansible) running on [Red Hat OpenShift Container Platform](https://www.redhat.com/en/technologies/cloud-computing/openshift) to automatically recover failed nodes.

1. There are some pseudo nodes in the environment. The node randomly sends SNMP Traps indicating normal or abnormal conditions. When the node sends an abnormal trap, it stops sending traps until it is recovered via a corresponding ansible playbook.

2. The traps sent from the pseudo nodes are received by [Red Hat Integration - Camel K](https://access.redhat.com/documentation/ja-jp/red_hat_integration/2022.q3)([Apache Camel K](https://camel.apache.org/camel-k/1.10.x/index.html)) integration, converted into a specific data format and written into [Red Hat AMQ Streams](https://access.redhat.com/documentation/en-us/red_hat_amq_streams/2.2)([Apache Kafka](https://kafka.apache.org/)).

3. [ansible-rulebook](https://github.com/ansible/ansible-rulebook) runs with Red Hat AMQ Streams (Apache Kafka) as the event source. It executes playbooks according to the content of the event, namely converted trap data, and recovers the failed pseudo node.

![overview](images/0.png)

## Requirements

* A Cluster built on Red Hat OpenShift Container Platform 4.8 or later

## Setup
1. Install operators

* Login to the Red Hat OpenShift Container Platform web console and install Red Hat Integration - Camel K Operator and Red Hat AMQ Streams Operator from OperatorHub.

![Red Hat Integration - Camel K Operator](images/1.png)
![Red Hat AMQ Streams Operator](images/2.png)
![Operators Installed](images/3.png)

2. Log into the Red Hat OpenShift cluster with oc command in your terminal
```
$ oc login ...
$ oc new-project eda
```

3. Clone the repository
```
$ git clone https://gitlab.com/m-muraki/event-driven-ansible-demo.git
```

4. Execute the following command in the `event-driven-ansible-demo/02_amqstreams` directory
```
$ oc apply -f 01_kafka_cluster.yaml
```

5. Periodically execute the following commands to check until all `STATUS` are `Running`
```
$ oc get pods | grep kafka-cluster
kafka-cluster-entity-operator-5657ccb89b-7fmj8   3/3     Running     0          51m
kafka-cluster-kafka-0                            1/1     Running     0          51m
kafka-cluster-kafka-1                            1/1     Running     0          51m
kafka-cluster-kafka-2                            1/1     Running     0          51m
kafka-cluster-zookeeper-0                        1/1     Running     0          51m
kafka-cluster-zookeeper-1                        1/1     Running     0          51m
kafka-cluster-zookeeper-2                        1/1     Running     0          51m
```

6. Execute the following command in the `event-driven-ansible-demo/03_camel_k` directory
```
$ oc apply -f 01_trap-receiver.yaml
```

7. Periodically execute the following commands to check the `PHASE` of the `trap-receiver` until it's `Running`
```
$ oc get integrations
NAME            PHASE     KIT                        REPLICAS
trap-receiver   Running   kit-cdb800olaujrto7pqq90   1
```

8. Execute the following command in the `event-driven-ansible-demo/04_ansible-rulebook-demo` directory
```
$ oc apply -f 01_serviceaccount.yaml
$ oc adm policy add-scc-to-user edascc -z edasa -n eda
$ oc apply -f 02_ansible-rulebook.yaml
$ oc apply -f 03_kafka-ui.yaml
$ oc apply -f 04_node1.yaml
$ oc apply -f 05_node2.yaml
$ oc apply -f 06_node3.yaml
```

9. Periodically execute the following commands to check until all `STATUS` are `Running`
```
$ oc get pods | grep -v build
NAME                                             READY   STATUS      RESTARTS   AGE
ansible-rulebook-demo-5cf646b45c-mvt7q           1/1     Running     0          47s
kafka-cluster-entity-operator-5657ccb89b-7fmj8   3/3     Running     0          51m
kafka-cluster-kafka-0                            1/1     Running     0          51m
kafka-cluster-kafka-1                            1/1     Running     0          51m
kafka-cluster-kafka-2                            1/1     Running     0          51m
kafka-cluster-zookeeper-0                        1/1     Running     0          51m
kafka-cluster-zookeeper-1                        1/1     Running     0          51m
kafka-cluster-zookeeper-2                        1/1     Running     0          51m
kafka-ui-6dfd9457b6-2jt5d                        1/1     Running     0          46s
node1-7975bd7f59-2cxdl                           1/1     Running     0          45s
node2-7989c78786-28gk6                           1/1     Running     0          44s
node3-bbc96969c-s7rkf                            1/1     Running     0          44s
trap-receiver-9cff56fc6-v5xmd                    1/1     Running     0          16m
```

## Run the demo

1. Open a new tab in your browser and open the ansible-rulebook-demo log in the Red Hat OpenShift Container Platform web console
![ansible-rulebook-demo logs](images/4.png)

2. Open three new tabs in your browser and open the logs from node1 to node3, also in web console

In order to show the logs of nodes, please execute `journalctl -f` in their terminal tab instead of logs tab.

![node1 logs](images/5.png)
![node2 logs](images/6.png)
![node3 logs](images/7.png)

3. Execute the following command in the `event-driven-ansible-demo` directory in your terminal
```
$ ./startdemo.sh
```

After a few seconds, the log screen of ansible-rulebook-demo in the browser will show events based on SNMP traps generated by node1-3 and show logs that the playbook was executed as a result of the event matching a rule.

![ansible-rulebook-demo logs](images/8.png)

The log screen for node1 to node3 in the browser will show logs indicating that traps have been sent, it went down and recovered.

![node1 logs](images/9.png)
![node2 logs](images/10.png)
![node3 logs](images/11.png)

Additionally, events in the kafka cluster can be observed in [kafka-ui](https://github.com/provectus/kafka-ui).
![kafka-ui](images/12.png)
![kafka-ui](images/13.png)

## Etc.

This demonstration uses [kafka-ui](https://github.com/provectus/kafka-ui) to see events.
