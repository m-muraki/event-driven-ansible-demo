#!/bin/bash

oc get pod -o template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}' | egrep "^node" | while read pod; do oc exec ${pod} -- touch /tmp/boot; done
