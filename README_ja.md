# Event Driven Ansible demo

## 概要

[Red Hat OpenShift Container Platform](https://www.redhat.com/en/technologies/cloud-computing/openshift)上で[Event Driven Ansible](https://github.com/ansible/event-driven-ansible)を動作させ、故障したノードを自動回復させるデモです。
このデモでは、以下の動作を確認することができます。

1. 環境上には、疑似ノードが数台存在します。この疑似ノードはランダムに正常あるいは異常を示すSNMP Trapを送出します。異常Trapを送出した場合、ansibleによって回復されるまで動作が停止します。
2. SNMP Trapは[Red Hat Integration - Camel K](https://access.redhat.com/documentation/ja-jp/red_hat_integration/2022.q3)([Apache Camel K](https://camel.apache.org/camel-k/1.10.x/index.html))インテグレーションが受信し、データ形式が変換され[Red Hat AMQ Streams](https://access.redhat.com/documentation/en-us/red_hat_amq_streams/2.2)([Apache Kafka](https://kafka.apache.org/))に書き込まれます。
3. [ansible-rulebook](https://github.com/ansible/ansible-rulebook)はRed Hat AMQ Streams(Apache Kafka)をイベントソースとしてansible playbookを起動します。イベントの内容に応じたplaybookが起動され、故障した疑似ノードを回復させます。

![overview](images/0.png)

## 要件

* Red Hat OpenShift Container Platform 4.8以上で構築されたクラスタ

## セットアップ
1. Operatorをインストールします。

* Red Hat OpenShift Container Platformのweb consoleにログインし、OperatorHubからRed Hat Integration - Camel K OperatorおよびRed Hat AMQ Streams Operatorをインストールします。
![Red Hat Integration - Camel K Operator](images/1.png)
![Red Hat AMQ Streams Operator](images/2.png)
![Operators Installed](images/3.png)

2. ターミナルで、ocコマンドを使いRed Hat OpenShiftのクラスタにログインします。
```
$ oc login ...
$ oc new-project eda
```

3. リポジトリをcloneします。
```
$ git clone https://gitlab.com/m-muraki/event-driven-ansible-demo.git
```

4. `event-driven-ansible-demo/02_amqstreams` ディレクトリで以下のコマンドを実行します。
```
$ oc apply -f 01_kafka_cluster.yaml
```

5. 全てのSTATUSがRunningになるまで、以下のコマンドを定期的に実行して確認します。
```
$ oc get pods | grep kafka-cluster
kafka-cluster-entity-operator-5657ccb89b-7fmj8   3/3     Running     0          51m
kafka-cluster-kafka-0                            1/1     Running     0          51m
kafka-cluster-kafka-1                            1/1     Running     0          51m
kafka-cluster-kafka-2                            1/1     Running     0          51m
kafka-cluster-zookeeper-0                        1/1     Running     0          51m
kafka-cluster-zookeeper-1                        1/1     Running     0          51m
kafka-cluster-zookeeper-2                        1/1     Running     0          51m
```

6. `event-driven-ansible-demo/03_camel_k` ディレクトリで、以下のコマンドを実行します。
```
$ oc apply -f 01_trap-receiver.yaml
```

7. trap-receiverのPHASEがRunningになるまで、以下のコマンドを定期的に実行して確認します。
```
$ oc get integrations
NAME            PHASE     KIT                        REPLICAS
trap-receiver   Running   kit-cdb800olaujrto7pqq90   1
```

8. `event-driven-ansible-demo/04_ansible-rulebook-demo` ディレクトリで以下のコマンドを実行します。
```
$ oc apply -f 01_serviceaccount.yaml
$ oc adm policy add-scc-to-user edascc -z edasa -n eda
$ oc apply -f 02_ansible-rulebook.yaml
$ oc apply -f 03_kafka-ui.yaml
$ oc apply -f 04_node1.yaml
$ oc apply -f 05_node2.yaml
$ oc apply -f 06_node3.yaml
```

9. 全てのSTATUSがRunningになるまで、以下のコマンドを定期的に実行して確認します。
```
$ oc get pods | grep -v build
NAME                                             READY   STATUS      RESTARTS   AGE
ansible-rulebook-demo-5cf646b45c-mvt7q           1/1     Running     0          47s
kafka-cluster-entity-operator-5657ccb89b-7fmj8   3/3     Running     0          51m
kafka-cluster-kafka-0                            1/1     Running     0          51m
kafka-cluster-kafka-1                            1/1     Running     0          51m
kafka-cluster-kafka-2                            1/1     Running     0          51m
kafka-cluster-zookeeper-0                        1/1     Running     0          51m
kafka-cluster-zookeeper-1                        1/1     Running     0          51m
kafka-cluster-zookeeper-2                        1/1     Running     0          51m
kafka-ui-6dfd9457b6-2jt5d                        1/1     Running     0          46s
node1-7975bd7f59-2cxdl                           1/1     Running     0          45s
node2-7989c78786-28gk6                           1/1     Running     0          44s
node3-bbc96969c-s7rkf                            1/1     Running     0          44s
trap-receiver-9cff56fc6-v5xmd                    1/1     Running     0          16m
```

以上でセットアップは完了です。

## デモ実行

1. ブラウザで新しいタブを開き、Red Hat OpenShift Container Platformのweb consoleでansible-rulebook-demoのログを開きます。
![ansible-rulebook-demo logs](images/4.png)

2. ブラウザで新しいタブを3つ開き、同じくweb consoleでnode1からnode3のログを開きます。
![node1 logs](images/5.png)
![node2 logs](images/6.png)
![node3 logs](images/7.png)

3. ターミナルの `event-driven-ansible-demo` ディレクトリで以下を実行します。
```
$ ./startdemo.sh
```

数秒待つと、ansible-rulebook-demoのログ画面にnode1から3で発生したSNMP Trapを基にしたイベントや、そのイベントがルールとマッチした結果playbookが実行されたことを示すログが表示されます。
![ansible-rulebook-demo logs](images/8.png)

node1からnode3のログ画面では、Trapの送信や回復が行われたことを示すログが表示されます。
![node1 logs](images/9.png)
![node2 logs](images/10.png)
![node3 logs](images/11.png)

また、[kafka-ui](https://github.com/provectus/kafka-ui)によってクラスタ内のイベントをグラフィカルに確認することができます。
![kafka-ui](images/12.png)
![kafka-ui](images/13.png)

## その他

本デモでは、イベントの確認に[kafka-ui](https://github.com/provectus/kafka-ui)を利用しています。
